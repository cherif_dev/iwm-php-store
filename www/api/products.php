<?php
session_start();
require_once('../db/pdo.php');
header("Access-Control-Allow-Origin: *"); //this allows cors
header('Content-Type: application/json');

function _get($var, $default = null)
{
    $value = $_GET[$var];
    return isset($value) ?  $value : $default;
}
function MinMaxPrices()
{
    global $pdo;
    $query = "SELECT  min(price) as minp,max(price) as maxp from products   "; 
    $stmt = $pdo->prepare($query); 
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    return ["min" => floor(floatval($result['minp'])), "max" => ceil(floatval($result['maxp']))];
}
function counts($category = null, $price = [0, 1000])
{
    global $pdo;
    $query = "SELECT count(*) as c ,min(price) as minp,max(price) as maxp from products where price >= :pmin and price <= :pmax ";
    $pmin = $price[0];
    $pmax = $price[1];

    if ($category) {
        $query =  $query . " and  ref in ( select product_id from product_categories where category_id in ($category) )";
    }
    $stmt = $pdo->prepare($query);
    $stmt->bindValue(':pmin', $pmin);
    $stmt->bindValue(':pmax', $pmax);

    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    return intval($result['c']);
}

function getProducts($page = 1, $limit = 10, $category = null, $price = [0, 1000], $orders = ['price' => "asc"])
{
    global $pdo;
    $offset = ($page - 1) * $limit;
    $pmin = $price[0];
    $pmax = $price[1];

    $query = "SELECT * FROM products where price >= :pmin and price <= :pmax ";
    if ($category)
        $query =  $query . " and ref in ( select product_id from product_categories where category_id  in ($category)  )";
    $query =  $query .  "order by ";
    foreach ($orders as $key => $value) {
        $query =  $query . $key . " " . $value;
    }
    $query =  $query .  " limit  :l offset :o ";
    $stmt = $pdo->prepare($query);
    $stmt->bindValue(':pmin', $pmin, PDO::PARAM_INT);
    $stmt->bindValue(':pmax', $pmax, PDO::PARAM_INT);
    $stmt->bindValue(':l', $limit, PDO::PARAM_INT);
    $stmt->bindValue(':o', $offset, PDO::PARAM_INT);

    $data = array();

    if ($stmt->execute()) {
        while ($row = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
    }

    if (!empty($data)) {
        return ($data[0]);
    } else {
        return [];
    }
}

$action =  (_get('action', 'products'));
switch ($action) {
    case 'minmaxprices':
        print json_encode(MinMaxPrices());
        break;
    case 'products':
        $limit = intval(_get('limit', 10));
        $page = intval(_get('page', 1));
        $category =  (_get('category', null));
        $price =  explode('-', _get('price', '0-1000'));
        $_SESSION["order"] =  _get('order', 'ref_asc');
        list($k, $v) = explode('_', _get('order', 'ref_asc'));
        $order[$k] = $v;
        $products = getProducts($page, $limit, $category, $price, $order);
        $count_all  = counts($category, $price);
        $data = [];
        $mod = ($count_all % $limit);
        $data['page'] = intval($page);
        $data['pages'] = $mod == 0 ? (($count_all - $mod) / $limit) : (($count_all - $mod) / $limit) + 1;
        $data['limit'] = $limit;
        $data['count_all'] = $count_all;
        $data['products'] = $products;
        $data['count'] = count($products);
        print json_encode($data);
        break;
    default:
        # code...
        break;
}
