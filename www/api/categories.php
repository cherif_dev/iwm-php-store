<?php
require_once('../db/pdo.php');
header("Access-Control-Allow-Origin: *"); //this allows cors
header('Content-Type: application/json');

function _get($var, $default = null)
{
    $value = $_GET[$var];
    return isset($value) ?  $value : $default;
}
function counts()
{
    global $pdo;
    $stmt = $pdo->prepare("SELECT count(*) as c from categories  ");
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    return intval($result['c']);
}

function getCategories($page = 1, $limit = 1000)
{
    global $pdo;

    $offset = ($page - 1) * $limit;
    $query = "SELECT c.id ,c.name , COUNT(pc.product_id) as products 
                            FROM categories as c 
                            INNER JOIN product_categories as pc on  c.id = pc.category_id
                            GROUP BY  c.id ,c.name
                            order by products desc
                            limit  ? offset ? ";
    $stmt = $pdo->prepare($query);
    $stmt->bindValue(1, $limit, PDO::PARAM_INT);
    $stmt->bindValue(2, $offset, PDO::PARAM_INT);
    $data = array();

    if ($stmt->execute()) {
        while ($row = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
    }

    if (!empty($data)) {
        return ($data[0]);
    } else {
        return [];
    }
}

$Categories = getCategories();
$count_all  = counts();
$data = [];
$data['categories'] = $Categories;
$data['count'] = count($Categories);
print json_encode($data);
