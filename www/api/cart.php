<?php
session_start();
require_once('../db/pdo.php');
header("Access-Control-Allow-Origin: *"); //this allows cors
header('Content-Type: application/json');

function _get($var, $default = null)
{
    $value = $_GET[$var];
    return isset($value) ?  $value : $default;
}


$data = $_SESSION['cart_items'];

function updateCart(&$data)
{
    global $pdo;
    if (!$data) return;
    foreach ($data as $ref => $qte) {
        $query = "SELECT * FROM products where ref  = :ref ";
        $stmt = $pdo->prepare($query);
        $stmt->bindValue(':ref',  $ref, PDO::PARAM_STR);
        if ($stmt->execute()) {
            $data[$ref]['product']  = $stmt->fetch(PDO::FETCH_ASSOC);
        }
    }
};
function addProduct()
{
    $data = $_SESSION['cart_items'];
    $product = _get("product_ref", null);
    $qte = floatval(_get("qte", 1));
    if ($product && $qte > 0) {
        $old = $data[$product];
        if ($old) {
            $old["qte"] =  $old["qte"] +  $qte;
            $data[$product] = $old;
        } else {
            $data[$product] = ['qte' => $qte];
        }
    }
    updateCart($data);
    $_SESSION['cart_items'] = $data;
    return $data;
}
function updateProduct()
{
    $data = $_SESSION['cart_items'];
    $product = _get("product_ref", null);
    $qte = floatval(_get("qte", 1));
    if ($product && $qte > 0) {
        $old = $data[$product];
        if ($old) {
            $old["qte"] =   $qte;
            $data[$product] = $old;
        }
    }
    updateCart($data);
    $_SESSION['cart_items'] = $data;
    return $data;
}

function deleteProduct()
{
    $data = $_SESSION['cart_items'];
    $product = _get("product_ref", null);
    if ($product) {
        $old = $data[$product];
        if ($old) {
            unset($data[$product]);
        }
    }
    updateCart($data);
    $_SESSION['cart_items'] = $data;
    return $data;
}

$action =  (_get('action', 'list'));

switch ($action) {
    case 'add_product':
        print json_encode(addProduct());
        break;
    case 'update_product':
        print json_encode(updateProduct());
        break;
    case 'delete_product':
        print json_encode(deleteProduct());
        break;
    case 'list':
        # code...
        updateCart($data);
        print json_encode($data);
    default:
        # code...
        break;
}
