<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
    <style>
        .v-card--reveal {
            align-items: center;
            bottom: 0;
            justify-content: center;
            opacity: .8;
            position: absolute;
            width: 100%;
        }

        .centered-input input {
            text-align: center
        }

        .loader {
            width: 100%;
            height: 100vh;
            background-color: #edf7fd;
        }

        .loader {
            animation: spin 1s infinite linear;
            border: solid 2vmin transparent;
            border-radius: 50%;
            border-right-color: #09f;
            border-top-color: #09f;
            box-sizing: border-box;
            height: 20vmin;
            left: calc(50% - 10vmin);
            position: fixed;
            top: calc(50% - 10vmin);
            width: 20vmin;
            z-index: 1;
        }

        .loader:before {
            animation: spin 2s infinite linear;
            border: solid 2vmin transparent;
            border-radius: 50%;
            border-right-color: #3cf;
            border-top-color: #3cf;
            box-sizing: border-box;
            content: "";
            height: 16vmin;
            left: 0;
            position: absolute;
            top: 0;
            width: 16vmin;
        }

        .loader:after {
            animation: spin 3s infinite linear;
            border: solid 2vmin transparent;
            border-radius: 50%;
            border-right-color: #6ff;
            border-top-color: #6ff;
            box-sizing: border-box;
            content: "";
            height: 12vmin;
            left: 2vmin;
            position: absolute;
            top: 2vmin;
            width: 12vmin;
        }

        @keyframes spin {
            100% {
                transform: rotate(360deg);
            }
        }
    </style>
</head>

<body>
    <div class="loader">

    </div>
    <div id="app" style="display: none;">
        <v-app>
            <v-app-bar :clipped-left="$vuetify.breakpoint.lgAndUp" app color="primary" dark>

                <v-toolbar-title style="width: 350px">
                    <a href="/" class="white--text" style="text-decoration: none">
                        ENSAB-SHOP
                    </a>
                </v-toolbar-title>
                <v-spacer></v-spacer>
                <v-btn icon class="mx-2" @click="cartDialog = true">
                    <v-badge color="green" overlap>
                        <template v-slot:badge>
                            {{cartItemsCount}}
                        </template>
                        <v-icon>mdi-cart</v-icon>
                    </v-badge>
                </v-btn>
            </v-app-bar>
            <v-main class="blue-grey lighten-5">
                <template>
                    <div>
                        <v-container fluid>
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <v-card outlined>
                                        <v-card-title>Filters</v-card-title>
                                        <v-divider></v-divider>
                                        <v-card-title>Prix</v-card-title>
                                        <v-range-slider v-model="range" :max="prices.max" :min="prices.min" :height="10" class="align-center" dense @change="loadProducts()"></v-range-slider>
                                        <v-row class="pa-2" dense>
                                            <v-col cols="12" sm="5">
                                                <v-text-field :value="range[0]" label="Min" outlined dense @change="$set(range, 0, $event)"></v-text-field>
                                            </v-col>
                                            <v-col cols="12" sm="2">
                                                <p class="pt-2 text-center">TO</p>
                                            </v-col>
                                            <v-col cols="12" sm="5">
                                                <v-text-field :value="range[1]" label="Max" outlined dense @change="$set(range, 1, $event)"></v-text-field>
                                            </v-col>
                                        </v-row>
                                        <v-divider></v-divider>
                                        <v-card-title class="px-4 py-3">Catégories ({{selectedCategories.length}})</v-card-title>
                                        <v-text-field v-model="search" class="px-2 mb-1" hide-details rounded outlined label="Rechercher un catégorie" append-icon="mdi-magnify"></v-text-field>

                                        <v-card-text class="px-0 pt-0 " style="overflow-y: scroll;">
                                            <template>
                                                <v-list nav style="height: 350px;" cZ>
                                                    <v-list-item-group v-model="selectedCategories" color="primary" multiple>
                                                        <template v-for="(item, i) in filteredCategories">
                                                            <v-list-item :key="i" class="my-0">
                                                                <v-list-item-content>
                                                                    <v-list-item-title v-text="item.name"> </v-list-item-title>
                                                                </v-list-item-content>
                                                                <v-list-item-action>
                                                                    <v-badge v-text="item.products"></v-badge>
                                                                </v-list-item-action>
                                                            </v-list-item>
                                                            <v-divider v-if="i < filteredCategories.length-1"></v-divider>
                                                        </template>
                                                    </v-list-item-group>
                                                </v-list>
                                            </template>
                                        </v-card-text>
                                        <v-divider></v-divider>

                                        <v-card-title class="pb-0">Évaluation du client</v-card-title>
                                        <v-container class="pt-0" fluid>
                                            <v-checkbox append-icon="mdi-star" label="4 & plus" hide-details dense></v-checkbox>
                                            <v-checkbox append-icon="mdi-star" label="3 & plus" hide-details dense></v-checkbox>
                                            <v-checkbox append-icon="mdi-star" label="2 & plus" hide-details dense></v-checkbox>
                                            <v-checkbox append-icon="mdi-star" label="1 & plus" hide-details dense></v-checkbox>
                                        </v-container>
                                        <v-divider></v-divider>

                                    </v-card>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-12">

                                    <!-- <v-breadcrumbs class="pb-0" :items="breadcrums"></v-breadcrumbs> -->

                                    <v-row dense align="center">
                                        <v-col cols="12" sm="3">
                                            <small>Affichage de {{((page-1)*limit)+1}}-{{(page*limit) <= count_products ?(page*limit) : count_products  }} sur {{count_products}} produits</small>
                                        </v-col>
                                        <v-col cols="12" sm="6">
                                            <div class="text-center  ">
                                                <v-pagination v-model="page" :length="pages" :total-visible="6"></v-pagination>
                                            </div>
                                        </v-col>
                                        <v-col cols="12" sm="3">
                                            <v-select class="pa-0" v-model="order" :items="options" item-text="name" item-value="id" style="margin-bottom: -20px;" outlined dense @change="loadProducts"></v-select>
                                        </v-col>
                                    </v-row>

                                    <v-divider></v-divider>

                                    <div class="row text-center">
                                        <div class="col-md-3 col-sm-6 col-xs-12" :key="pro.id" v-for="pro in products">
                                            <v-hover v-slot:default="{ hover }">
                                                <v-card class="mx-auto" max-width="600" min-height="300">
                                                    <v-img class="align-end" :aspect-ratio="16/9" height="200px" :src="pro.image">

                                                        <v-expand-transition>
                                                            <div v-if="hover" class="d-flex transition-fast-in-fast-out white darken-2 v-card--reveal display-3 white--text" style="height: 100%;">
                                                                <v-btn v-if="hover" class="" outlined @click.stop="showProduct(pro)">VIEW</v-btn>
                                                            </div>

                                                        </v-expand-transition>
                                                    </v-img>
                                                    <v-card-text class="text--primary " style="    min-height: 100px;">
                                                        <div><a style="text-decoration: none">{{pro.name}}</a></div>
                                                        <div>${{pro.price}}</div>
                                                    </v-card-text>
                                                </v-card>
                                            </v-hover>
                                        </div>
                                    </div>
                                    <div class="text-center mt-12">
                                        <v-pagination v-model="page" :length="pages" :total-visible="6"></v-pagination>
                                    </div>
                                </div>
                            </div>
                        </v-container>
                        <v-dialog v-model="productDialog" persistent max-width="1200">
                            <template v-if="selectedProduct">
                                <v-card>
                                    <template>
                                        <v-fab-transition>
                                            <v-btn color="red" fab dark small absolute top right @click="productDialog=false" style="top:10px !important">
                                                <v-icon>mdi-close</v-icon>
                                            </v-btn>
                                        </v-fab-transition>
                                    </template>
                                    <v-container>
                                        <div class="row">
                                            <div class="col-md-5 col-sm-5 col-xs-12">
                                                <v-carousel>
                                                    <v-carousel-item :src="selectedProduct.image">
                                                    </v-carousel-item>

                                                </v-carousel>
                                            </div>
                                            <div class="col-md-7 col-sm-7 col-xs-12">
                                                <v-breadcrumbs class="pb-0" :items="breadcrums"></v-breadcrumbs>
                                                <div class="pl-6">
                                                    <p class="display-1 mb-0">{{selectedProduct.name}}</p>
                                                    <v-card-actions class="pa-0">
                                                        <p class="headline font-weight-bold pt-3">${{selectedProduct.price}} <del class="subtitle-1  font-weight-thin">${{parseFloat(selectedProduct.price*1.23).toFixed(0)}}.00</del></p>
                                                        <v-spacer></v-spacer>
                                                        <v-rating v-model="selectedProduct.rating" class="" background-color="warning lighten-3" color="warning" dense></v-rating>
                                                        <span class="body-2	font-weight-thin"> 25 REVIEWS</span>
                                                    </v-card-actions>
                                                    <p class="subtitle-1 font-weight-ligth">
                                                        {{selectedProduct.description}}
                                                    </p>

                                                    <v-text-field class="centered-input" prepend-inner-icon="mdi-minus" append-icon="mdi-plus" outlined style=" width:100px" v-model="selectedProductQte" dense @click:append="decreaseQte()" @click:prepend-inner="increaseQte() "></v-text-field>
                                                    <v-btn class="primary white--text" @click="addProduct">
                                                        <v-icon>mdi-cart</v-icon> Ajouter au panier
                                                    </v-btn>


                                                </div>

                                                </di>
                                            </div>
                                        </div>

                                    </v-container>
                                    <v-card class="accent">
                                        <v-container>
                                            <v-row no-gutters>
                                                <v-col class="col-12 col-md-4 col-sm-12">
                                                    <v-row>
                                                        <v-col class="col-12 col-sm-3 pr-4 hidden-sm-only" align="right">
                                                            <v-icon class="display-2">mdi-truck</v-icon>
                                                        </v-col>
                                                        <v-col class="col-12 col-sm-9 pr-4">
                                                            <h3 class="font-weight-light ">LIVRAISON ET RETOUR GRATUITS</h3>
                                                            <p class="font-weight-thin">Livraison gratuite à partir de 300 $</p>
                                                        </v-col>
                                                    </v-row>
                                                </v-col>
                                                <v-col class="col-12 col-md-4 col-sm-12">
                                                    <v-row>
                                                        <v-col class="col-12 col-sm-3 pr-4" align="right">
                                                            <v-icon class="display-2">mdi-cash-usd</v-icon>
                                                        </v-col>
                                                        <v-col class="col-12 col-sm-9 pr-4">
                                                            <h3 class="font-weight-light">GARANTIE DE REMBOURSEMENT</h3>
                                                            <p class="font-weight-thin">Garantie de remboursement de 30 jours</p>
                                                        </v-col>
                                                    </v-row>
                                                </v-col>
                                                <v-col class="col-12 col-md-4 col-sm-12">
                                                    <v-row>
                                                        <v-col class="col-12 col-sm-3 pr-4" align="right">
                                                            <v-icon class="display-2">mdi-headset</v-icon>
                                                        </v-col>
                                                        <v-col class="col-12 col-sm-9 pr-4">
                                                            <h3 class="font-weight-light">020-800-456-747</h3>
                                                            <p class="font-weight-thin">Assistance disponible 24h / 24 et 7j / 7</p>
                                                        </v-col>
                                                    </v-row>
                                                </v-col>
                                            </v-row>
                                        </v-container>
                                    </v-card>

                                </v-card>
                            </template>
                        </v-dialog>
                        <v-dialog v-model="cartDialog" persistent max-width="80%">
                            <template>
                                <v-card>
                                    <template>
                                        <v-fab-transition>
                                            <v-btn color="red" fab dark small absolute top right @click="cartDialog=false" style="top:10px !important">
                                                <v-icon>mdi-close</v-icon>
                                            </v-btn>
                                        </v-fab-transition>
                                    </template>
                                    <v-container>
                                        <p class="display-2 font-weight-light	text-center pa-4">VOTRE PANIER</p>
                                        <v-row>
                                            <v-col :cols="12" md="9" sm="12">
                                                <v-simple-table>
                                                    <template v-slot:default>
                                                        <thead>
                                                            <tr>
                                                                <th>Produit</th>
                                                                <th>Prix</th>
                                                                <th>Quantité</th>
                                                                <th>Total</th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr v-for="(item, objKey)  in cartItems">
                                                                <td>
                                                                    <v-list-item :key="objKey" @click="">
                                                                        <v-list-item-avatar>
                                                                            <v-img :src="item.product.image"></v-img>
                                                                        </v-list-item-avatar>

                                                                        <v-list-item-content>
                                                                            <v-list-item-title> {{item.product.ref}}</v-list-item-title>
                                                                            <v-list-item-subtitle> {{item.product.name}}</v-list-item-subtitle>
                                                                        </v-list-item-content>
                                                                    </v-list-item>
                                                                </td>
                                                                <td> {{item.product.price}}
                                                                </td>
                                                                <td>
                                                                    <v-text-field class="pt-10" label="Outlined" style="width: 80px;" single-line outlined v-model="item.qte" type="number" min="1" @change="updateProduct(objKey, item.qte)"></v-text-field>
                                                                </td>
                                                                <td>{{parseFloat(item.product.price * item.qte ).toFixed(2)}}</td>
                                                                <td>
                                                                    <v-btn icon color="red" @click="deleteFromCart(item.product)">
                                                                        <v-icon>mdi-delete</v-icon>
                                                                    </v-btn>
                                                                </td>
                                                            </tr>

                                                        </tbody>
                                                    </template>
                                                </v-simple-table>
                                            </v-col>
                                            <v-col :cols="12" md="3" sm="12" style="background-color: lightgray">
                                                <p class="headline">Récapitulatif de la commande</p>
                                                </p>
                                                <v-simple-table>
                                                    <template v-slot:default>
                                                        <tbody>
                                                            <tr>
                                                                <td>Sous-total de la commande</td>
                                                                <td class="text-right" style="width: 50px;">${{totals.subtotal}}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Frais d'expédition</td>
                                                                <td class="text-right" style="width: 50px;">${{totals.shipping}}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>TVA</td>
                                                                <td class="text-right" style="width: 50px;">${{totals.tax}}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Total</td>
                                                                <td class="text-right" style="width: 50px;"><b>${{totals.total}}</b></td>
                                                            </tr>
                                                        </tbody>
                                                    </template>
                                                </v-simple-table>
                                                <div class="text-center">
                                                    <v-btn class="primary white--text mt-5  " style="width: 100%;">PROCÉDER AU PAIEMENT</v-btn>
                                                    <v-btn @click.stop="cartDialog=false" class="secondary black--text mt-5 " style="width: 100%;">Continuer vos achats</v-btn>
                                                </div>
                                            </v-col>
                                        </v-row>
                                    </v-container>
                                    <v-card class="accent">
                                        <v-container>
                                            <v-row no-gutters>
                                                <v-col class="col-12 col-md-4 col-sm-12">
                                                    <v-row>
                                                        <v-col class="col-12 col-sm-3 pr-4 hidden-sm-only" align="right">
                                                            <v-icon class="display-2">mdi-truck</v-icon>
                                                        </v-col>
                                                        <v-col class="col-12 col-sm-9 pr-4">
                                                            <h3 class="font-weight-light">LIVRAISON ET RETOUR GRATUITS</h3>
                                                            <p class="font-weight-thin">Livraison gratuite à partir de 300 $</p>
                                                        </v-col>
                                                    </v-row>
                                                </v-col>
                                                <v-col class="col-12 col-md-4 col-sm-12">
                                                    <v-row>
                                                        <v-col class="col-12 col-sm-3 pr-4" align="right">
                                                            <v-icon class="display-2">mdi-cash-usd</v-icon>
                                                        </v-col>
                                                        <v-col class="col-12 col-sm-9 pr-4">
                                                            <h3 class="font-weight-light">GARANTIE DE REMBOURSEMENT</h3>
                                                            <p class="font-weight-thin">Garantie de remboursement de 30 jours</p>
                                                        </v-col>
                                                    </v-row>
                                                </v-col>
                                                <v-col class="col-12 col-md-4 col-sm-12">
                                                    <v-row>
                                                        <v-col class="col-12 col-sm-3 pr-4" align="right">
                                                            <v-icon class="display-2">mdi-headset</v-icon>
                                                        </v-col>
                                                        <v-col class="col-12 col-sm-9 pr-4">
                                                            <h3 class="font-weight-light">020-800-456-747</h3>
                                                            <p class="font-weight-thin">Assistance disponible 24h / 24 et 7j / 7</p>
                                                        </v-col>
                                                    </v-row>
                                                </v-col>
                                            </v-row>
                                        </v-container>
                                    </v-card>
                                </v-card>
                            </template>
                        </v-dialog>
                        <v-snackbar v-model="snackbar.visible" :multi-line="true" :color="snackbar.color" top :timeout="2000">
                            <h3>{{ snackbar.text }}</h3>

                            <template v-slot:action="{ attrs }">
                                <v-btn icon color="white" text v-bind="attrs" @click="snackbar.visible = false">
                                    <v-icon>mdi-close</v-icon>
                                </v-btn>
                            </template>
                        </v-snackbar>
                    </div>
                </template>
            </v-main>
        </v-app>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue@2.x/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <script>
        const vuetify = new Vuetify({
            theme: {
                themes: {
                    light: {
                        primary: '#3f51b5',
                        secondary: '#b0bec5',
                        accent: '#3ae3d6 ',
                        error: '#b71c1c',
                    },
                },
            },
        })
        new Vue({
            el: '#app',
            vuetify: vuetify,
            data: () => ({
                url: 'http://207.180.220.37:8089/api',
                range: [0, 0],
                snackbar: {
                    visible: false,
                    text: "",
                    color: "success"
                },
                order: '<?php echo $_SESSION["order"] ?>',
                options: [{
                        id: "ref_asc",
                        name: 'Popularité'
                    },
                    {
                        id: "price_asc",
                        name: 'Prix croissant'
                    },
                    {
                        id: "price_desc",
                        name: 'Prix décroissant'
                    }
                ],
                cartItems: [],
                cartItemsCount: '<?php echo isset($_SESSION['cart_items']) ? count($_SESSION['cart_items']) : 0 ?>',
                pages: 0,
                page: 1,
                limit: 12,
                count_products: 0,
                breadcrums: [],
                prices: {
                    min: 0,
                    max: 10000
                },
                products: [],
                selectedProduct: null,
                selectedProductQte: 1,
                categories: [],
                selectedCategories: [],
                search: "",
                productDialog: false,
                cartDialog: false,
            }),
            watch: {
                page() {
                    this.loadProducts()
                },
                selectedCategories() {
                    this.loadProducts()
                },
                range() {
                    console.log({
                        range: this.range
                    })
                }
            },
            computed: {
                filteredCategories() {
                    console.log({
                        s: this.search
                    })
                    return this.search.length > 1 ? this.categories.filter(c => c.name.toLowerCase().includes(this.search.toLowerCase())) : this.categories;
                },
                totals() {
                    let subtotal = 0,
                        shipping = 0,
                        tax = 0,
                        total = 0;
                    for (const [key, value] of Object.entries(this.cartItems)) {
                        console.log(value.product.price)
                        subtotal += parseFloat(value.qte * value.product.price);
                        shipping += parseFloat(value.product.shipping);
                    }
                    tax = subtotal * 0.2;
                    total = tax + subtotal;
                    console.log(subtotal)
                    return {
                        subtotal: subtotal.toFixed(2),
                        shipping: shipping,
                        tax: tax.toFixed(2),
                        total: total.toFixed(2),
                    };
                }
            },
            mounted() {
                document.querySelector("#app").style.display = "block";
                document.querySelector(".loader").style.display = "none";
                this.init();
                this.loadCategories()
                this.loadProducts()
            },
            methods: {
                showProduct(pro) {
                    this.selectedProduct = pro;
                    this.selectedProductQte = 1;
                    this.productDialog = true;
                },
                increaseQte() {
                    this.selectedProductQte = this.selectedProductQte > 1 ? this.selectedProductQte - 1 : 1
                },
                decreaseQte() {
                    console.log({
                        d: this.selectedProduct
                    })
                    this.selectedProductQte = this.selectedProductQte < 10 ? this.selectedProductQte + 1 : 10

                },
                async init() {
                    this.minMaxPrices()
                    this.loadCartItems();
                },
                async minMaxPrices() {
                    let path = `${this.url}/products.php?action=minmaxprices`
                    const res = await axios.get(path);
                    this.range = [res.data.min, res.data.max]
                    this.prices = res.data
                },
                async loadCartItems() {
                    let path = `${this.url}/cart.php?action=list`
                    const res = await axios.get(path);
                    if (res.data) {
                        this.cartItems = res.data
                        this.cartItemsCount = Object.entries(res.data).length;
                    }
                },
                async minMaxPrices() {
                    let path = `${this.url}/products.php?action=minmaxprices`
                    const res = await axios.get(path);
                    this.range = [res.data.min, res.data.max]
                    this.prices = res.data
                },
                async loadProducts() {
                    let path = `${this.url}/products.php?action=products&page=${this.page}&limit=${this.limit}`
                    const cats = this.selectedCategories.map(s => `'${this.filteredCategories[s].id}'`)
                    if (cats.length > 0)
                        path = path + `&category=${cats.join(',')}`
                    if (this.range[1] > 0)
                        path = path + `&price=${this.range.join('-')}`
                    if (this.order)
                        path = path + `&order=${this.order}`
                    const res = await axios.get(path);
                    this.products = res.data.products
                    this.pages = res.data.pages
                    this.page = this.pages < this.page ? this.pages : this.page;
                    this.count_products = res.data.count_all
                },
                async loadCategories() {
                    const path = `${this.url}/categories.php`
                    const res = await axios.get(path);
                    this.categories = res.data.categories
                },
                async addProduct() {
                    const path = `${this.url}/cart.php?action=add_product&product_ref=${this.selectedProduct.ref}&qte=${this.selectedProductQte}`
                    try {
                        const res = await axios.get(path);
                        this.cartItems = res.data
                        this.cartItemsCount = Object.entries(this.cartItems).length
                        this.snackbar.text = `«${this.selectedProduct.name}» a été ajouté à votre panier.`;
                        this.snackbar.color = "success";
                    } catch (error) {
                        this.snackbar.text = `Une erreur s'est produite veuillez réessayer`;
                        this.snackbar.color = "error";
                    }
                    this.snackbar.visible = true;
                },
                async updateProduct(ref, qte) {
                    const path = `${this.url}/cart.php?action=update_product&product_ref=${ref}&qte=${qte}`
                    try {
                        const res = await axios.get(path);
                        this.cartItems = res.data
                        this.cartItemsCount = Object.entries(this.cartItems).length
                        this.snackbar.text = ` Votre panier a été modifiée avec succés.`;
                        this.snackbar.color = "success";
                    } catch (error) {
                        this.snackbar.text = `Une erreur s'est produite veuillez réessayer`;
                        this.snackbar.color = "error";
                    }
                    this.snackbar.visible = true;
                },
                async deleteFromCart(product) {
                    const path = `${this.url}/cart.php?action=delete_product&product_ref=${product.ref}`
                    try {
                        const res = await axios.get(path);
                        Vue.delete(this.cartItems, product.ref);
                        this.snackbar.text = `«${product.name}» a été supprimé de votre panier.`;
                        this.snackbar.color = "success";
                        this.cartItemsCount--;
                    } catch (error) {
                        console.log(error)
                        this.snackbar.text = `Une erreur s'est produite veuillez réessayer`;
                        this.snackbar.color = "error";
                    }
                    this.snackbar.visible = true;
                }
            }
        })
    </script>
</body>

</html>