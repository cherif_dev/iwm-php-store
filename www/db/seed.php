<?php
require_once('./pdo.php');
$stmt = $pdo->prepare("INSERT INTO products VALUES (?,?,?,?,?,?,?,?)");

$content = file_get_contents('products.json');

$products = json_decode($content);

foreach ($products as $p) {
  //prepare statement for inserting product
  $stmt->bindValue(1, $p->ref, PDO::PARAM_STR);
  $stmt->bindValue(2, $p->name, PDO::PARAM_STR);
  $stmt->bindValue(3, $p->description, PDO::PARAM_STR);
  $stmt->bindValue(4, $p->type, PDO::PARAM_STR);
  $stmt->bindValue(5, $p->manufacturer, PDO::PARAM_STR);
  $stmt->bindValue(6, $p->image, PDO::PARAM_STR);
  $stmt->bindValue(7, $p->shipping, PDO::PARAM_STR);
  $stmt->bindValue(8, $p->price, PDO::PARAM_STR);
  $stmt->execute();
}

$stmt = $pdo->prepare("INSERT INTO categories VALUES (?,?)");
$stmt_pc = $pdo->prepare("INSERT INTO product_categories VALUES (?,?)");

foreach ($products as $p) {
  foreach ($p->category as $c) {
    //prepare statement for inserting category
    $stmt->bindValue(1, $c->id, PDO::PARAM_STR);
    $stmt->bindValue(2, $c->name, PDO::PARAM_STR);

    try {
      $stmt->execute();
      echo "<br> category inserted  " . $c->id . " " . $c->name . " <br>";
    } catch (PDOException $Exception) {
      echo " <br>" . (int)$Exception->getCode() . " - " . $Exception->getMessage() . " <br>";
    }
    //prepare statement for inserting product categories
    $stmt_pc->bindValue(1, $p->ref, PDO::PARAM_STR);
    $stmt_pc->bindValue(2, $c->id, PDO::PARAM_STR);
    try {
      $stmt_pc->execute();
      echo "<br>product_categories inserted  " . $c->id . " " . $c->name . " <br>";
    } catch (PDOException $Exception) {
      echo " <br>" . (int)$Exception->getCode() . " - " . $Exception->getMessage() . " <br>";
    }
  }
}
